# Menu des applications

{% hint style="info" %}
Section en construction 🚧
{% endhint %}



## Écran de lancement de l'ordinateur

{% @figma/embed fileId="YmJ8kC43NWf7F3lCGPxGEI" nodeId="1195:119843" url="https://www.figma.com/file/YmJ8kC43NWf7F3lCGPxGEI/Primtux---Interfaces?mode=design&node-id=1195:119843&t=d7KwCRs4jxUqF7z1-1&type=design" %}



## Choix des sessions

{% @figma/embed fileId="YmJ8kC43NWf7F3lCGPxGEI" nodeId="1195:119854" url="https://www.figma.com/file/YmJ8kC43NWf7F3lCGPxGEI/Primtux---Interfaces?mode=design&node-id=1195:119854&t=d7KwCRs4jxUqF7z1-1&type=design" %}

### Mode clair

{% @figma/embed fileId="YmJ8kC43NWf7F3lCGPxGEI" nodeId="1195:119855" url="https://www.figma.com/file/YmJ8kC43NWf7F3lCGPxGEI/Primtux---Interfaces?mode=design&node-id=1195:119855&t=d7KwCRs4jxUqF7z1-1&type=design" %}



## Session "Koda"

{% @figma/embed fileId="YmJ8kC43NWf7F3lCGPxGEI" nodeId="1195:121438" url="https://www.figma.com/file/YmJ8kC43NWf7F3lCGPxGEI/Primtux---Interfaces?mode=design&node-id=1195:121438&t=d7KwCRs4jxUqF7z1-1&type=design" %}

{% @figma/embed fileId="YmJ8kC43NWf7F3lCGPxGEI" nodeId="1212:87869" url="https://www.figma.com/file/YmJ8kC43NWf7F3lCGPxGEI/Primtux---Interfaces?mode=design&node-id=1212:87869&t=d7KwCRs4jxUqF7z1-1&type=design" %}

### Mode clair

{% @figma/embed fileId="YmJ8kC43NWf7F3lCGPxGEI" nodeId="1432:49952" url="https://www.figma.com/file/YmJ8kC43NWf7F3lCGPxGEI/Primtux---Interfaces?mode=design&node-id=1432:49952&t=d7KwCRs4jxUqF7z1-1&type=design" %}
