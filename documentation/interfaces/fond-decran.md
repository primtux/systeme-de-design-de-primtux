# Fond d'écran

{% hint style="info" %}
Le fond d'écran d'ordinateur de Primtux est disponible en 2 tailles pour le format jpeg et une version vectorielle.
{% endhint %}

<figure><img src="../.gitbook/assets/Primtux-Fond_d_ecran.jpg" alt="Fond d&#x27;écran d&#x27;ordinateur Primtux"><figcaption></figcaption></figure>

### Télécharger les fichiers

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/illustrations/fond_d_ecran" %}

