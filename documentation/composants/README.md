---
cover: ../.gitbook/assets/Building blocks-pana.svg
coverY: 44
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# 🧱 Composants

Bienvenue dans la section "Composants" de notre Système de Design, une ressource essentielle pour simplifier et accélérer le processus de conception d'interfaces.&#x20;

Ici, vous découvrirez une collection complète de composants prêts à l'emploi, soigneusement élaborés pour répondre à une variété de besoins de conception. Des boutons interactifs aux formulaires complexes, ces composants sont conçus pour être réutilisables, facilitant ainsi la création d'interfaces esthétiques et fonctionnelles. En adoptant ces éléments pré-construits, vous gagnerez en efficacité tout en garantissant une cohérence visuelle à travers toutes les sections de votre application.&#x20;

Explorez cette section pour découvrir comment les composants de notre Système de Design peuvent simplifier votre processus de développement et contribuer à la création d'interfaces utilisateur engageantes et homogènes.



<table data-view="cards"><thead><tr><th></th><th data-hidden data-card-cover data-type="files"></th><th data-hidden></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><h2>🚧 Aide</h2></td><td></td><td></td><td></td><td><a href="aide.md">aide.md</a></td></tr><tr><td><h2>Avatar et sessions</h2></td><td><a href="../.gitbook/assets/Avatars.png">Avatars.png</a></td><td></td><td></td><td><a href="avatars-et-sessions.md">avatars-et-sessions.md</a></td></tr><tr><td><h2>Barre de recherche</h2></td><td><a href="../.gitbook/assets/Barre de recherche.png">Barre de recherche.png</a></td><td></td><td></td><td><a href="barre-de-recherche.md">barre-de-recherche.md</a></td></tr><tr><td><h2>Boutons</h2></td><td><a href="../.gitbook/assets/Boutons.png">Boutons.png</a></td><td></td><td></td><td><a href="page-2.md">page-2.md</a></td></tr><tr><td><h2>Catégories</h2></td><td><a href="../.gitbook/assets/Categories.png">Categories.png</a></td><td></td><td></td><td><a href="categories.md">categories.md</a></td></tr><tr><td><h2>🚧 Champs de saisie</h2></td><td></td><td></td><td></td><td><a href="champs-de-saisie.md">champs-de-saisie.md</a></td></tr><tr><td><h2>Icônes d'applications</h2></td><td><a href="../.gitbook/assets/Icones app.png">Icones app.png</a></td><td></td><td></td><td><a href="icones-dapplication.md">icones-dapplication.md</a></td></tr></tbody></table>
