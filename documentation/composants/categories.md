# Catégories

## Catégorisation des applications

Les applications sont rangées par catégories et sous-catégories dans le menu principal de Primtux.&#x20;

Ces dernières possèdent des illustrations permettant d’identifier visuellement le type d’outils et activités disponibles. Les catégories sont construites par les membres de l’association, en prenant en compte les programmes scolaires de l’Éducation Nationale.

La classification des applications dans les différentes catégories est présentée dans le document Figma ci-dessous :

{% @figma/embed fileId="o0F0FAVFGqiLtAeaBculcn" nodeId="805:114579" url="https://www.figma.com/file/o0F0FAVFGqiLtAeaBculcn/Primtux-Design-System---Composants?mode=design&node-id=805:114579&t=3AH1qfYOeQRqi8X0-1&type=design" %}

***

Le composant catégorie et ses différentes tailles et états sont présentés dans le document Figma ci-dessous :&#x20;

{% @figma/embed fileId="o0F0FAVFGqiLtAeaBculcn" nodeId="805:12017" url="https://www.figma.com/file/o0F0FAVFGqiLtAeaBculcn/Primtux-Design-System---Composants?mode=design&node-id=805:12017&t=tCNUiZxR0JF5TWLk-1&type=design" %}

{% @figma/embed fileId="o0F0FAVFGqiLtAeaBculcn" nodeId="847:13120" url="https://www.figma.com/file/o0F0FAVFGqiLtAeaBculcn/Primtux-Design-System---Composants?mode=design&node-id=847:13120&t=VSlQEFDpBcz0d7DB-1&type=design" %}
