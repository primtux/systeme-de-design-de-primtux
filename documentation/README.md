# Le Système de Design de Primtux

**Communiquer sur Primtux, concevoir et développer des applications, en utilisant des éléments prêts à l'emploi.**

{% hint style="info" %}
Un Système de Design (ou Design System) est une méthodologie holistique qui vise à unifier l'expérience de l'utilisateur à travers toutes les interfaces et à renforcer la cohérence de la marque. Pour mieux comprendre le principe, lire la section : [#quest-ce-quun-systeme-de-design](./#quest-ce-quun-systeme-de-design "mention")
{% endhint %}

Notre Système de Design repose sur une organisation pensée en quatre groupes distincts :

<table data-card-size="large" data-view="cards"><thead><tr><th></th><th></th><th data-hidden></th><th data-hidden data-type="content-ref"></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-card-cover data-type="files"></th></tr></thead><tbody><tr><td><h2>Fondamentaux</h2></td><td><p>Cette section constitue le socle sur lequel repose l'identité visuelle de Primtux. Les typographies, échelles typographiques, échelles d'espacement, logotype, illustrations, icônes et palettes de couleurs forment un ensemble cohérent et unifié. </p><p></p><p>Ces fondamentaux définissent les codes visuels de la marque, garantissant une esthétique homogène à travers toutes les interfaces et supports de communication.</p><p></p><p><mark style="color:blue;">Découvrir</mark> 👉</p></td><td></td><td><a href="fondamentaux/logotype.md">logotype.md</a></td><td><a href="fondamentaux/">fondamentaux</a></td><td><a href=".gitbook/assets/Construction crane-pana.svg">Construction crane-pana.svg</a></td></tr><tr><td><h2>Composants</h2></td><td><p>Les composants prêts à l'emploi facilitent la construction d'interfaces. De simples boutons à des formulaires complexes, ces éléments réutilisables accélèrent significativement le processus de conception et de développement. </p><p></p><p>L'intérêt réside dans la cohérence visuelle et la rapidité de mise en œuvre, renforçant ainsi l'efficacité de la création d'interfaces.<br><br><br><mark style="color:blue;">Découvrir</mark> 👉</p></td><td></td><td></td><td><a href="composants/">composants</a></td><td><a href=".gitbook/assets/Building blocks-pana.svg">Building blocks-pana.svg</a></td></tr><tr><td><h2>Interfaces</h2></td><td><p>Les interfaces représentent l'épine dorsale du système en offrant des modèles et des composants spécifiques. Ces éléments charnières assurent une mise en page cohérente et une expérience utilisateur harmonieuse. </p><p></p><p>En simplifiant la création d'interfaces homogènes, cette section garantit une navigation fluide au sein de l'application, renforçant ainsi la satisfaction utilisateur.</p><p><br><mark style="color:blue;">Découvrir</mark> 👉</p></td><td><mark style="color:blue;">Découvrir</mark> 👉</td><td></td><td><a href="interfaces/">interfaces</a></td><td><a href=".gitbook/assets/Content structure-pana.svg">Content structure-pana.svg</a></td></tr><tr><td><h2>Communication</h2></td><td><p>Cet espace dédié à la communication propose des ressources prêtes à l'emploi, comme des flyers, affiches, et autres éléments promotionnels. </p><p></p><p>Cela permet de maintenir une identité visuelle forte dans tous les supports de communication, renforçant ainsi la reconnaissance de la marque et la cohérence globale de la communication.<br><br><br><mark style="color:blue;">Découvrir</mark> 👉</p></td><td></td><td></td><td><a href="communication/">communication</a></td><td><a href=".gitbook/assets/Product presentation-pana.svg">Product presentation-pana.svg</a></td></tr></tbody></table>

[_Illustrations by Storyset_](https://storyset.com/online)

{% hint style="info" %}
Chaque groupe répond à des besoins spécifiques, formant ainsi un écosystème qui facilite la conception, la mise en œuvre et la promotion de Primtux de manière harmonieuse et qualitative.
{% endhint %}

***

## Qu'est-ce qu'un Système de Design ?

Un Système de Design est une collection structurée d'éléments de conception réutilisables, tels que des composants, des directives de style, des modèles et des interactions, qui forment la base du langage visuel d'une marque. C'est un ensemble de règles et de normes qui garantissent la cohérence et la fluidité à travers tous les points de contact avec l'utilisateur, que ce soit sur un site web, une application mobile, ou même des supports marketing.

### Composants clés d'un Système de Design

* **Palette de couleurs :** Définit les couleurs de la marque pour garantir une utilisation cohérente à travers toutes les interfaces.
* **Typographie :** Spécifie les polices de caractères utilisées, les tailles de texte, les marges, et d'autres éléments liés à la présentation du texte.
* **Échelle des espacements :** Détermine les espaces entre les différents éléments de l'interface, garantissant une mise en page équilibrée et une expérience visuelle agréable.
* **Composants d'interface :** Englobe les boutons, les formulaires, les icônes, et d'autres éléments d'interface récurrents.
* **Règles de navigation :** Guide l'utilisateur à travers l'application ou le site web de manière cohérente et intuitive.

{% hint style="info" %}
Notre Système de Design propose également des supports et des ressources pour faciliter la communication et la promotion de Primtux.
{% endhint %}

## Pourquoi un Système de Design ?

* **Cohérence :** Assure une expérience utilisateur homogène, renforçant ainsi la reconnaissance de la marque.
* **Efficacité :** Accélère le processus de conception et de développement en fournissant des éléments pré-construits et des lignes directrices.
* **Évolutivité :** Facilite les mises à jour et les évolutions de l'interface sans compromettre la cohérence visuelle.
* **Collaboration :** Fournit un langage commun aux équipes de conception et de développement.
* **Économie de Temps et de Ressources :** Réduit le temps nécessaire à la résolution de problèmes récurrents, notamment sur l'accessibilité ou la communication.

C'est un outil stratégique qui façonne l'expérience de l'utilisateur, renforce la présence de la marque, accélère le processus de conception et de développement. Le Système de Design de Primtux est étudié pour permettre de construire des interfaces ergonomiques et accessibles.

### Utilisabilité

Les composants d'interface sont conçus de manière à favoriser une utilisation intuitive, minimisant les frictions et améliorant l'efficacité de l'expérience utilisateur. L'analyse des parcours utilisateur et des comportements d'interaction est cruciale pour garantir que le Système de Design répond aux besoins réels des utilisateurs.

### Accessibilité

L'accessibilité est une priorité dans la conception du Système de Design de Primtux. Les interfaces doivent être conçues de manière à être utilisables par tous, indépendamment des capacités physiques ou sensorielles. Cela inclut par exemple des choix de couleurs respectueux de la lisibilité, des textes facilement compréhensibles et une structure de navigation logique.

### Mode sombre / mode clair

Le système de design de Primtux offre une expérience visuelle adaptative avec le mode sombre et le mode clair. Le mode sombre, caractérisé par des tons plus sombres d'arrière-plan et des éléments lumineux, favorise une lisibilité accrue dans des environnements peu éclairés tout en réduisant la fatigue oculaire. En revanche, le mode clair, avec des arrière-plans lumineux et des textes sombres, est idéal dans des conditions d'éclairage plus intense. Cette fonctionnalité permet aux utilisateurs de personnaliser leur expérience, garantissant un confort visuel optimal en fonction de leurs préférences individuelles et des conditions ambiantes.

{% hint style="info" %}
<mark style="color:blue;">**Le mode par défaut de Primtux est le mode sombre.**</mark>
{% endhint %}

