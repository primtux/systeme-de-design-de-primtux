# Prospectus

{% hint style="info" %}
Le flyer est disponible en deux version de fichier : \
\
\- **Flyer\_impression.pdf** est destiné à être envoyé aux imprimeurs. Il comporte des fonds perdus qui sont découpés par l'imprimeur après impression. Cela permet d'avoir le fond coloré qui vient jusqu'au bord du perspectus, en évitant ainsi d'avoir un rebord blanc.\
Le flyer a été conçu pour être imprimé en format A5.\
\
\- **Flyer\_numerique.pdf** est une version utilisable pour la lecture sur support numérique, sans fonds perdu qui sont peu esthétiques pour une lecture sur support numérique.
{% endhint %}

### Version pour impression

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/blob/5736fd48189cc475d4d2964b0b2a81d444a34ea3/fichiers/communication/Flyer_impression.pdf" %}

### Version pour lecture numérique

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/blob/5736fd48189cc475d4d2964b0b2a81d444a34ea3/fichiers/communication/Flyer_numerique.pdf" %}
