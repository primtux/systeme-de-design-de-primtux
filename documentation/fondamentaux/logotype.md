---
description: Les logos et icônes de Primtux représentent le personnage du même nom.
---

# Logotype

## Logo principal

Le logo principal de Primtux a pour but de représenter visuellement la marque et de faciliter son identification dans le paysage numérique. Il sert à établir une identité forte et reconnaissable et assurer la cohérence de la marque sur différents supports.

Le logo doit être utilisé conformément aux directives pour préserver son intégrité et son impact.

{% hint style="info" %}
**Règles d'utilisation du logo**

Pour garantir une utilisation cohérente et qualitative du logo de Primtux, veuillez respecter les règles suivantes :

* **Choix de la version :** Utilisez la version horizontale par défaut. Optez pour la version verticale si l’espace alloué sur le support est restreint. Pour l'utilisation du logo dans l'entête d'un site internet, prendre la version horizontale.
* **Contraste :** Sélectionnez la version fond clair pour les fonds blancs ou clairs et la version fond sombre pour les fonds colorés ou sombres, afin de maximiser le contraste pour une meilleure lisibilité. Toujours utiliser le logo qui offre le plus fort contraste avec le fond.
* **Intégrité du logo :** Ne modifiez pas, ne distorsionnez pas et ne recadrez pas le logo. Il doit être affiché tel qu’il est fourni, pour maintenir l’intégrité de la marque.
* **Espace de protection :** Laissez toujours un espace suffisant autour du logo pour qu'il soit clairement séparé des autres éléments graphiques ou du texte.
{% endhint %}

### Horizontal

<figure><img src="../.gitbook/assets/Mise_en_situation-Logo_horizontal-Fond_clair.svg" alt="Logo Primtux horizontal pour fond clair"><figcaption></figcaption></figure>

<figure><img src="../.gitbook/assets/Mise_en_situation-Logo_horizontal-Fond_sombre.svg" alt="Logo Primtux vertical pour fond clair"><figcaption></figcaption></figure>

### Vertical

<div>

<figure><img src="../.gitbook/assets/Mise_en_situation-Logo_vertical-Fond_clair.svg" alt="Logo Primtux vertical pour fond clair" width="258"><figcaption></figcaption></figure>

 

<figure><img src="../.gitbook/assets/Mise_en_situation-Logo_vertical-Fond_sombre.svg" alt="Logo Primtux vertical pour fond clair" width="258"><figcaption></figcaption></figure>

</div>

### Télécharger les logos

{% hint style="info" %}
Les versions vectorielles (.svg) sont à privilégier pour l'utilisation des logos, car elles permettent de conserver la qualité de l'image dans toutes les tailles.
{% endhint %}

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/logo/svg" %}

#### Versions bitmap (.png)

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/logo/png" %}

***

## Icônes

Les icônes servent à identifier l'application du Primtux menu, l'élément central pour accéder aux applications.

**L'icône avec le fond transparent est celle à utiliser par défaut.** L'icône avec le fond coloré est destinée à être utilisé sur les éventuels systèmes d'exploitation qui ne prennent pas en charge le vectoriel (svg) et ajoutent automatiquement un fond noir sur les images transparentes.

<div align="left" data-full-width="false">

<figure><img src="https://framagit.org/primtux/systeme-de-design-de-primtux/-/raw/Main/fichiers/icones/svg/Icone_Primtux-Defaut-Fond_transparent.svg?ref_type=heads" alt="" width="188"><figcaption></figcaption></figure>

 

<figure><img src="https://framagit.org/primtux/systeme-de-design-de-primtux/-/raw/Main/fichiers/icones/svg/Icone_Primtux-Variante-Fond_colore.png.svg?ref_type=heads" alt="" width="188"><figcaption></figcaption></figure>

</div>

{% hint style="info" %}
**Règles d'utilisation**

* Utiliser la version fond transparent par défaut, sauf quand le système ajoute automatiquement un fond et ne prend pas en charge le format SVG.
* Ne pas déformer l'icône.
{% endhint %}

### Favicon

![Favicon Primtux](https://framagit.org/primtux/systeme-de-design-de-primtux/-/raw/Main/fichiers/icones/svg/Favicon\_Primtux.svg?ref\_type=heads)

La favicon, ou icône de favori, est une petite icône généralement de taille 16x16 ou 32x32 pixels, qui représente un site web ou une page web. Elle s'affiche dans la barre d'adresse du navigateur, à côté du nom du site dans les onglets ou dans la liste des favoris, permettant aux utilisateurs de reconnaître facilement le site web lorsqu'ils ont plusieurs onglets ouverts ou lorsqu'ils parcourent leurs marque-pages. Utiliser la favicon Primtux, sur tous les supports internet de Primtux, aide à construire l'identité visuelle et améliore l'expérience utilisateur en facilitant la navigation et la reconnaissance du site.

### Télécharger les icônes

{% hint style="info" %}
Les versions vectorielles (.svg) des icônes doivent être privilégiés, car elles permettent de conserver la qualité de l'image dans toutes les tailles.
{% endhint %}

#### Versions vectorielles (.svg)

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/icones/svg?ref_type=heads" %}

#### Versions bitmap (.png)

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/icones/png?ref_type=heads" %}

***

## Avatar réseaux sociaux

<div align="left">

<figure><img src="https://framagit.org/primtux/systeme-de-design-de-primtux/-/raw/Main/fichiers/avatar-reseaux-sociaux/Avatar_Primtux-Reseaux_sociaux.svg?ref_type=heads" alt="Avatar Primtux pour les réseaux sociaux" width="188"><figcaption></figcaption></figure>

</div>

Pour assurer une présence cohérente de Primtux sur les plateformes de réseaux sociaux, un avatar officiel qui respecte la charte graphique est utilisé. Utilisez l'avatar Primtux pour représenter la marque partout sur les réseaux sociaux.

{% hint style="info" %}
**Règles d'utilisation**

* Ne pas recadrer l'image à l'importation sur la plateforme de réseau social, le maximum de de l'image doit être visible. Selon la plateforme, l'image carrée sera automatiquement transformé en rond.&#x20;
* Ne pas déformer l'avatar lors du redimensionnement.
{% endhint %}

### **Télécharger l'avatar**

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/avatar-reseaux-sociaux?ref_type=heads" %}

***

### Document source (Figma)

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="812:9418" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?type=design&node-id=812%3A9418&mode=design&t=SfLFOWvWt7TdS4KJ-1" %}
