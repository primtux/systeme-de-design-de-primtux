# Espacements

## Échelle des espacements

L'échelle des espacements dans notre Système de Design repose sur une base de 8 pixels, offrant une approche systématique et modulaire pour la disposition harmonieuse des éléments de l'interface. Chaque multiple de 8 pixels est soigneusement calculé pour assurer une distance équilibrée entre les composants, favorisant une mise en page propre et bien proportionnée.

Cette approche basée sur une base de 8 pixels simplifie le processus de conception et facilite la collaboration entre designers et développeurs en offrant une cohérence visuelle et une facilité d'ajustement. En adoptant cette échelle, nous visons à créer une interface esthétiquement équilibrée tout en facilitant l'alignement et la structuration cohérente des éléments, contribuant ainsi à une expérience utilisateur fluide et agréable.

Les design tokens correspondants aux espacements sont décrits dans le document Figma ci-dessous :

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="249:79" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=249:79&t=xzuuPeIeutDXoaFX-1&type=design" %}
