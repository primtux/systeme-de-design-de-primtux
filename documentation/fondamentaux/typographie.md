---
layout:
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Typographie

La typographie est un élément fondamental qui façonne l'identité visuelle et renforce la lisibilité de Primtux sur une multitude de supports.&#x20;

***

## Police d'écriture

La police d'écriture de l’identité de PrimTux est la **Luciole**, qui est conçue spécifiquement pour améliorer l'expérience visuelle des personnes malvoyantes.

> Ce projet est le résultat de plus de deux années de collaboration entre le Centre Technique Régional pour la Déficience Visuelle et le studio typographies.fr. Le projet a bénéficié d'une bourse de la Fondation suisse Ceres et de l'appui du laboratoire DIPHE de l'Université Lumière Lyon 2. \
> [_luciole-vision.com_](http://luciole-vision.com/)

{% hint style="info" %}
L’utilisation de la police Luciole dans le système de design de Primtux souligne l’engagement envers l'accessibilité du projet. Ainsi, nous contribuons à créer un environnement visuel accueillant pour toutes et tous.
{% endhint %}

**Pour télécharger la police d'écriture utilisée sur Primtux :**&#x20;

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/police-luciole" %}

Pour plus de détails sur la police et son installation : [luciole-vision.com](http://luciole-vision.com/#download).

***

## Échelle typographique

Une échelle typographique est un système hiérarchisé de tailles de caractères utilisé de manière cohérente dans la conception graphique. Elle définit les différentes tailles de texte pour différents éléments d'une interface, tels que les titres, sous-titres, paragraphes, etc. L'objectif principal de l'échelle typographique est d'assurer une hiérarchie visuelle claire, d'optimiser la lisibilité et de créer une harmonie visuelle dans la mise en page.

Deux échelles typographiques sont utilisées pour la gestion des titres, afin de s’adapter aux différentes tailles d’écrans. L’échelle de 1,25 est utilisée pour les titre sur desktop ou tablette, tandis que l’échelle 1,125 est adaptée aux écrans de téléphones mobiles. L’échelle 1,125 est également utilisé pour les styles de textes (hors titres), sur mobile et desktop.



{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="218:1293" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=218:1293&t=5I2ZFvy03T7ApXr6-1&type=design" %}

***

## Styles de texte

L'importance de définir différents styles de texte réside dans la création d'une hiérarchie visuelle, permettant aux utilisateurs de naviguer et de comprendre le contenu de manière efficace.&#x20;

Des titres accrocheurs aux paragraphes informatifs, chaque style est soigneusement calibré pour refléter l'importance sémantique du contenu. En utilisant des balises HTML appropriées, telles que `<h1>` pour les titres principaux, nous nous assurons de respecter les normes de sémantique web. Cela ne contribue pas seulement à une meilleure compréhension par les moteurs de recherche, mais aussi à une accessibilité améliorée pour tous les utilisateurs.

Les valeurs en rem sont à utiliser en développement, afin de favoriser le responsive et l’accessibilité des interfaces. Les pixels indiqués sont en calculés sur une base de 1rem = 16px.



{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="221:1706" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=221:1706&t=xzuuPeIeutDXoaFX-1&type=design" %}

***

{% hint style="info" %}
Section en construction 🚧
{% endhint %}

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="237:549" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=237:549&t=xzuuPeIeutDXoaFX-1&type=design" %}
