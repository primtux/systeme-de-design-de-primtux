# Tokens

{% hint style="info" %}
Section en construction 🚧
{% endhint %}

{% hint style="info" %}
Un token est une variable, relié à une valeur qui peut être par exemple une couleur, une taille ou une autre variable.
{% endhint %}

Dans le Système de Design de Primtux, notre approche repose sur l'utilisation de deux types de design tokens : les "Primitives tokens" et les "Semantic tokens". Les "Primitives tokens" jouent le rôle de références fondamentales en regroupant des valeurs de base telles que les couleurs, les échelles d'espacements et les arrondis. Ces éléments essentiels forment la base de notre système de design, assurant la cohérence et la flexibilité dans toute l'interface.

D'un autre côté, les "Semantic tokens" établissent des liens significatifs en reliant les "Primitives tokens" à des noms de token qui correspondent à des utilisations spécifiques en UX. Par exemple, ces tokens peuvent être attribués à des éléments tels que la couleur d'interaction principale, la couleur de survol, etc. Cette approche permet une gestion centralisée et efficace des éléments de design, facilitant les mises à jour et les ajustements tout en garantissant une interprétation claire des valeurs par l'équipe de conception et de développement.

En adoptant cette logique de tokens, notre Design System favorise une méthodologie modulaire et évolutive, simplifiant la maintenance, encourageant la cohérence visuelle, et facilitant l'adaptation aux évolutions futures de l'interface. Cette approche réfléchie renforce la robustesse et la flexibilité de notre système de design, contribuant ainsi à une expérience utilisateur homogène et bien pensée.
