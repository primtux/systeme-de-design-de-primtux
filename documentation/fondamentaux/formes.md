# Formes

Nous privilégions des formes à coins arrondis, en particulier le rectangle arrondi, avec une échelle basée sur des multiples de 4 pixels.&#x20;

Cette approche vise à créer une esthétique accueillante, sécurisante et ludique, parfaitement adaptée à un public jeune. Les formes arrondies éliminent les bords abrupts, favorisant une apparence conviviale tout en facilitant une navigation intuitive. Ce choix de formes arrondies contribue à une expérience visuelle positive, encouragent l'exploration, et renforce l'aspect engageant de notre application pour les enfants.



{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="255:115" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=255:115&t=xzuuPeIeutDXoaFX-1&type=design" %}
