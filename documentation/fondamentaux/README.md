---
cover: ../.gitbook/assets/Construction crane-pana.svg
coverY: 155
layout:
  cover:
    visible: true
    size: hero
  title:
    visible: true
  description:
    visible: true
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# 🏛 Fondamentaux

Bienvenue dans la section "Fondamentaux" de notre Système de Design, le cœur de notre identité visuelle. Ici, vous découvrirez les éléments essentiels qui définissent l'esthétique cohérente de Primtux.&#x20;

Des choix de typographies aux échelles typographiques qui guident notre communication écrite, en passant par les échelles d'espacement pour une disposition équilibrée, chaque élément contribue à la construction d'une identité visuelle distinctive.&#x20;

Notre logotype, nos illustrations, nos icônes et notre palette de couleurs complètent cet ensemble, créant ainsi une fondation solide pour garantir la cohérence visuelle à travers toutes les interfaces. Explorez cette section pour plonger au cœur des choix esthétiques qui définissent notre Système de Design, renforçant ainsi l'unicité et la reconnaissance de Primtux.



<table data-view="cards"><thead><tr><th></th><th data-hidden></th><th data-hidden></th><th data-hidden data-card-target data-type="content-ref"></th><th data-hidden data-card-cover data-type="files"></th></tr></thead><tbody><tr><td><h2>Logotype</h2></td><td></td><td></td><td><a href="logotype.md">logotype.md</a></td><td></td></tr><tr><td><h2>Typographie</h2></td><td></td><td></td><td><a href="typographie.md">typographie.md</a></td><td><a href="../.gitbook/assets/Typographies.png">Typographies.png</a></td></tr><tr><td><h2>Couleurs</h2></td><td></td><td></td><td><a href="couleurs.md">couleurs.md</a></td><td><a href="../.gitbook/assets/Couleurs.png">Couleurs.png</a></td></tr><tr><td><h2>Espacements</h2></td><td></td><td></td><td><a href="espacements.md">espacements.md</a></td><td><a href="../.gitbook/assets/Espacements.png">Espacements.png</a></td></tr><tr><td><h2>Formes</h2></td><td></td><td></td><td><a href="formes.md">formes.md</a></td><td><a href="../.gitbook/assets/Formes.png">Formes.png</a></td></tr><tr><td><h2>Illustrations</h2></td><td></td><td></td><td><a href="illustrations.md">illustrations.md</a></td><td><a href="../.gitbook/assets/Illustrations.png">Illustrations.png</a></td></tr><tr><td><h2>Icônes</h2></td><td></td><td></td><td><a href="icones.md">icones.md</a></td><td><a href="../.gitbook/assets/Icones.png">Icones.png</a></td></tr><tr><td><h2>Tokens</h2></td><td></td><td></td><td><a href="tokens.md">tokens.md</a></td><td></td></tr></tbody></table>
