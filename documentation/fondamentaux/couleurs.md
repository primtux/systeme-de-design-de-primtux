# Couleurs

{% hint style="info" %}
Section en construction 🚧
{% endhint %}

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="244:296" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=244:296&t=giwb8lHk0kZIibTa-1&type=design" %}

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="441:5509" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=441:5509&t=giwb8lHk0kZIibTa-1&type=design" %}

***

## Design tokens

Pour comprendre ce qu'est un token, comment ça fonctionne et comment les utiliser, consulter notre page dédiée :&#x20;

{% content-ref url="tokens.md" %}
[tokens.md](tokens.md)
{% endcontent-ref %}

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="601:32836" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=601:32836&t=giwb8lHk0kZIibTa-1&type=design" %}

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="812:6292" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=812:6292&t=giwb8lHk0kZIibTa-1&type=design" %}
