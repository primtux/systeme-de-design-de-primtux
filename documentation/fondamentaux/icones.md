# Icônes

Notre univers visuel est enrichi par la riche librairie Material. Nos icônes, sélectionnées dans la bibliothèque [Google Material Icons](https://fonts.google.com/icons), apportent une esthétique cohérente et contemporaine à nos interfaces.

Pour garantir une intégration fluide et une manipulation facile, nous préconisons l'utilisation de paramètres spécifiques. Optez pour le style "Rounded" avec le remplissage “Fill” désactivé, une taille optique de 24, un grade de 0 (par défaut), et une épaisseur de 400.

Cette configuration assure une uniformité visuelle et une adaptabilité optimale dans l'ensemble de notre interface. Explorez cette section pour accéder à une panoplie d'icônes conçues pour améliorer l'expérience utilisateur, offrant à notre application une identité visuelle élégante et moderne.



{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="725:1143" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=725:1143&t=giwb8lHk0kZIibTa-1&type=design" %}
