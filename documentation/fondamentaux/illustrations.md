# Illustrations

## Les personnages

Bienvenue dans l'univers captivant de notre application, où l'apprentissage devient une aventure stimulante, guidée par une charmante galerie de personnages uniques.&#x20;



<div align="left">

<figure><img src="../.gitbook/assets/Jerry la souris.svg" alt="Jerry la souris" width="104"><figcaption></figcaption></figure>

</div>

Pour les plus jeunes du cycle 1, **Jerry** la souris offre une compagnie ludique et encourageante, transformant l'exploration éducative en une expérience mémorable.



<div align="left">

<figure><img src="../.gitbook/assets/Koda le koala.svg" alt="Koda le koala" width="96"><figcaption></figcaption></figure>

</div>

Poursuivant cette quête d'apprentissage, le cycle 2 est accueilli par **Koda** le koala, apportant sa douceur et son enthousiasme.&#x20;



<div align="left">

<figure><img src="../.gitbook/assets/Leon le cameleon.svg" alt="Léon le caméléon" width="179"><figcaption></figcaption></figure>

</div>

**Léon** le caméléon, avec sa curiosité infinie, se présente aux élèves du cycle 3, les incitant à explorer le savoir avec émerveillement.



<div align="left">

<figure><img src="../.gitbook/assets/Poe la pieuvre.svg" alt="Poe la pieuvre" width="293"><figcaption></figcaption></figure>

</div>

Enfin, **Poe** la pieuvre incarne la personnalisation pour les professeurs, offrant un espace dédié pour adapter le système à leurs besoins spécifiques.&#x20;



<div align="left">

<figure><img src="../.gitbook/assets/Primtux.svg" alt="Primtux le manchot" width="148"><figcaption></figcaption></figure>

</div>

Tout au long de cette aventure éducative, le manchot **Primtux**, notre guide dévoué, assure une cohérence et une convivialité constantes, offrant un fil conducteur à chaque étape de l'apprentissage.&#x20;

Explorez notre application en compagnie de ces personnages attachants, chacun apportant une dimension unique à l'expérience éducative.



***

### Télécharger les fichiers des personnages

{% hint style="info" %}
Les versions vectorielles (.svg) sont à privilégier pour l'utilisation des illustrations, car elles permettent de conserver la qualité de l'image dans toutes les tailles.

Pour chaque personnage, il existe une version pour fond sombre et une autre pour fond clair. Ceci permet d'avoir la couleur de l'ombre du personnage qui soit adaptée à la luminosité du fond utilisé.
{% endhint %}

#### Versions vectorielles (.svg)

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/illustrations/personnages/svg" %}

#### Versions bitmap (.png)

{% embed url="https://framagit.org/primtux/systeme-de-design-de-primtux/-/tree/Main/fichiers/illustrations/personnages/png" %}

***

### Document source (Figma)

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="410:195" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?type=design&node-id=410%3A195&mode=design&t=wdSto4Hqd2ItrfN2-1" %}

***

## Les domaines d'apprentissage

Pour plus de détail sur les domaines d'apprentissage, consulter la page dédiée :&#x20;

{% content-ref url="../composants/categories.md" %}
[categories.md](../composants/categories.md)
{% endcontent-ref %}



### Document source (Figma)

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="686:10034" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=686:10034&t=giwb8lHk0kZIibTa-1&type=design" %}

{% @figma/embed fileId="Jy3ilUsmovKkgeX8QeoGFE" nodeId="734:1134" url="https://www.figma.com/file/Jy3ilUsmovKkgeX8QeoGFE/Primtux-Design-System---Fondamentaux?mode=design&node-id=734:1134&t=giwb8lHk0kZIibTa-1&type=design" %}
