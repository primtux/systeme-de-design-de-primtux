# 💡 Utilisation

Pour retrouver les éléments de communication pour promouvoir Primtux, rendez-vous sur cette page :&#x20;

{% content-ref url="../communication/" %}
[communication](../communication/)
{% endcontent-ref %}

{% hint style="info" %}
Section en construction 🚧
{% endhint %}
